#!/usr/bin/env python
# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Run LCM interface display daemon."""

import argparse
import logging

from chameleond.display_server import DisplayServer


def main():
  """The Main program, to run LCM interface display daemon."""
  parser = argparse.ArgumentParser(
      description='Launch a Chameleon daemon.',
      formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('--log', type=str, dest='log', metavar='PATH',
                      default='/var/log/displayd',
                      help='write log to this file or - to stderr')
  parser.add_argument('--chameleon_host', type=str, dest='host',
                      default='0.0.0.0',
                      help='host address of RPC server')
  parser.add_argument('--chameleon_port', type=int, dest='port', default=9992,
                      help='port number of RPC server')
  parser.add_argument('-v', '--verbose', action='count', dest='verbose',
                      help='increase message verbosity')

  options = parser.parse_args()

  verbosity_map = {0: logging.INFO, 1: logging.DEBUG}
  verbosity = verbosity_map.get(options.verbose or 0, logging.NOTSET)
  log_format = '%(asctime)s %(levelname)s '
  if options.verbose > 0:
    log_format += '(%(filename)s:%(lineno)d) '
  log_format += '%(message)s'

  log_config = {'level': verbosity,
                'format': log_format}
  if options.log != '-':
    log_config.update({'filename': options.log})
  logging.basicConfig(**log_config)

  try:
    DisplayServer(options.host, options.port).RunServer()
  except Exception as e:  # pylint: disable=W0702
    logging.exception('Failed to start Display server.')
    logging.exception(e)
    exit(1)


if __name__ == '__main__':
  main()
