/* Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

/* TPM Lightweight Command Library.
 *
 * A low-level library for interfacing to TPM hardware or an emulator.
 */

#ifndef TPM_LITE_TLCL_H_
#define TPM_LITE_TLCL_H_

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define POSSIBLY_UNUSED __attribute__((unused))

#ifdef __STRICT_ANSI__
#define INLINE
#else
#define INLINE inline
#endif

/* Outputs an error message and quits the program.
 */
POSSIBLY_UNUSED
static void error(const char *format, ...) {
  va_list ap;
  va_start(ap, format);
  fprintf(stderr, "ERROR: ");
  vfprintf(stderr, format, ap);
  va_end(ap);
  exit(1);
}

/* Outputs a warning and continues.
 */
POSSIBLY_UNUSED
static void warning(const char *format, ...) {
  va_list ap;
  va_start(ap, format);
  fprintf(stderr, "WARNING: ");
  vfprintf(stderr, format, ap);
  va_end(ap);
}

#define assert(expr) do { if (!(expr)) { \
      error("assert fail: %s at %s:%d\n", \
            #expr, __FILE__, __LINE__); }} while(0)

/* Call this first.
 */
void TlclLibInit(void);

/* Close and open the device.  This is needed for running more complex commands
 * at user level, such as TPM_TakeOwnership, since the TPM device can be opened
 * only by one process at a time.
 */
void TlclCloseDevice(void);
void TlclOpenDevice(void);

/* Logs to stdout.  Arguments like printf.
 */
void TlclLog(char* format, ...);

/* Sets the log level.  0 is quietest.
 */
void TlclSetLogLevel(int level);

/* Sends a TPM_Startup(ST_CLEAR).  Note that this is a no-op for the emulator,
 * because it runs this command during initialization.  The TPM error code is
 * returned (0 for success).
 */
uint32_t TlclStartup(void);

/* Run the self test.  Note---this is synchronous.  To run this in parallel
 * with other firmware, use ContinueSelfTest.  The TPM error code is returned.
 */
uint32_t TlclSelftestfull(void);

/* Runs the self test in the background.
 */
uint32_t TlclContinueSelfTest(void);

/* Defines a space with permission [perm].  [index] is the index for the space,
 * [size] the usable data size.  The TPM error code is returned.
 */
uint32_t TlclDefineSpace(uint32_t index, uint32_t perm, uint32_t size);

/* Defines a space with permission [perm].  [index] is the index for the space,
 * [size] the usable data size.  Returns the TPM error code.
 */
uint32_t TlclDefineSpaceResult(uint32_t index, uint32_t perm, uint32_t size);

/* Writes [length] bytes of [data] to space at [index].  The TPM error code is
 * returned.
 */
uint32_t TlclWrite(uint32_t index, uint8_t *data, uint32_t length);

/* Reads [length] bytes from space at [index] into [data].  The TPM error code
 * is returned.
 */
uint32_t TlclRead(uint32_t index, uint8_t *data, uint32_t length);

/* Write-locks space at [index].  The TPM error code is returned.
 */
uint32_t TlclWriteLock(uint32_t index);

/* Read-locks space at [index].  The TPM error code is returned.
 */
uint32_t TlclReadLock(uint32_t index);

/* Asserts physical presence in software.  The TPM error code is returned.
 */
uint32_t TlclAssertPhysicalPresence(void);

/* Turns off physical presence and locks it off until next reboot.  The TPM
 * error code is returned.
 */
uint32_t TlclLockPhysicalPresence(void);

/* Sets the nvLocked bit.  The TPM error code is returned.
 */
uint32_t TlclSetNvLocked(void);

/* Returns 1 if the TPM is owned, 0 otherwise.
 */
int TlclIsOwned(void);

/* Issues a ForceClear.  The TPM error code is returned.
 */
uint32_t TlclForceClear(void);

/* Issues a PhysicalEnable.  The TPM error code is returned.
 */
uint32_t TlclSetEnable(void);

/* Issues a PhysicalDisable.  The TPM error code is returned.
 */
uint32_t TlclClearEnable(void);

/* Issues a SetDeactivated.  Pass 0 to activate.  Returns result code.
 */
uint32_t TlclSetDeactivated(uint8_t flag);

/* Gets flags of interest.  (Add more here as needed.)  The TPM error code is
 * returned.
 */
uint32_t TlclGetFlags(uint8_t* disable, uint8_t* deactivated);

/* Sets the bGlobalLock flag, which only a reboot can clear.  The TPM error
 * code is returned.
 */
uint32_t TlclSetGlobalLock(void);

/* Performs a TPM_Extend.
 */
uint32_t TlclExtend(int pcr_num, uint8_t* in_digest, uint8_t* out_digest);

/* Gets the permission bits for the NVRAM space with |index|.
 */
uint32_t TlclGetPermissions(uint32_t index, uint32_t* permissions);

#endif  /* TPM_LITE_TLCL_H_ */
