Systém Chrome OS chybí nebo je poškozen.
Vložte jednotku USB nebo SD kartu pro obnovení.
Vložte jednotku USB pro obnovení.
Vložte SD kartu nebo jednotku USB pro obnovení. (Poznámka: modrý port USB pro obnovení NELZE použít.)
Vložte jednotku USB do jednoho ze čtyř portů na ZADNÍ straně zařízení.
Vložené zařízení neobsahuje systém Chrome OS.
Ověření operačního systému je VYPNUTO.
Stisknutím MEZERNÍKU ho obnovíte.
Stisknutím klávesy ENTER potvrdíte, že chcete zapnout ověření operačního systému.
Systém se restartuje a smažou se místní data.
Chcete-li se vrátit zpět, stiskněte klávesu ESC.
Ověření operačního systému je ZAPNUTO.
Chcete-li ověření operačního systému VYPNOUT, stiskněte klávesu ENTER.
Nápovědu naleznete na adrese https://google.com/chromeos/recovery
Kód chyby
Chcete-li zahájit obnovení, odeberte všechna připojená zařízení.
Model 60061e
Chcete-li vypnout ověření operačního systému, stiskněte tlačítko RECOVERY (Obnovení).
Připojený zdroj napájení nemá pro toto zařízení dostatečný výkon.
Systém Chrome OS se nyní vypne.
Použijte prosím správný adaptér a zkuste to znovu.
Odeberte všechna připojená zařízení a zahajte obnovení.
Chcete-li vybrat alternativní bootloader, stiskněte numerickou klávesu:
Diagnostiku spustíte stisknutím vypínače.
Chcete-li vypnout ověření operačního systému, stiskněte tlačítko napájení.
