Možnosti za razvijalce
Prikaz podatkov o odpravljanju napak
Omogočanje preverjanja operacijskega sistema
Izklop
Jezik
Zagon iz omrežja
Zagon z Legacy BIOS
Zagona s pogona USB
Zagon s ključka USB ali kartice SD
Zagon z notranjega diska
Prekliči
Potrditev omogočanja preverjanja operacijskega sistema
Onemogočanje preverjanja operacijskega sistema
Potrditev onemogočanja preverjanja operacijskega sistema
Z gumboma za glasnost se pomikajte gor ali dol,
z gumbom za vklop pa izberite možnost.
Če onemogočite preverjanje operacijskega sistema, sistem NE BO VAREN.
Izberite »Prekliči«, če želite ohraniti zaščito.
Preverjanje operacijskega sistema je IZKLOPLJENO. Sistem NI VAREN.
Izberite »Omogočanje preverjanja operacijskega sistema«, če želite obnoviti zaščito.
Izberite »Potrditev omogočanja preverjanja operacijskega sistema«, če želite zaščititi sistem.
