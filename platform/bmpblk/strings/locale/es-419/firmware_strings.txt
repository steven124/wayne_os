Se dañó o no se instaló el Sistema operativo Chrome.
Inserta una tarjeta SD o un dispositivo de memoria USB de recuperación.
Inserta un dispositivo de memoria USB de recuperación.
Inserta un dispositivo de memoria USB o una tarjeta SD de recuperación (ten en cuenta que el puerto USB azul NO funcionará para la recuperación del sistema).
Inserta un dispositivo de memoria USB de recuperación en alguno de los 4 puertos de la parte POSTERIOR del dispositivo.
El dispositivo que insertaste no contiene el Sistema operativo Chrome.
La verificación del SO está DESACTIVADA
Presiona ESPACIO para volver a habilitarla.
Presiona INTRO para confirmar que deseas activar la verificación del SO.
Se reiniciará el sistema y se borrarán los datos locales.
Para volver hacia atrás, presiona ESC.
La verificación del SO está ACTIVADA.
Para DESACTIVAR la verificación del SO, presiona INTRO.
Para obtener ayuda, visita https://google.com/chromeos/recovery
Código de error
Quita todos los dispositivos externos para iniciar la recuperación.
Modelo 60061e
Para DESACTIVAR la verificación del SO, presiona el botón RECUPERACIÓN.
La fuente de alimentación que se conectó no tiene energía suficiente para hacer funcionar este dispositivo.
El Sistema operativo Chrome se cerrará.
Usa el adaptador correcto y vuelve a intentarlo.
Quita todos los dispositivos conectados y comienza la recuperación.
Presiona una tecla numérica para seleccionar un bootloader alternativo:
Presiona el botón de encendido para realizar un diagnóstico.
Para desactivar la verificación del SO, presiona el botón de encendido.
