# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
from fuzzy_check import FuzzyCheck
from validators import *

# originally generated gestures:
# FlingStop
#   Motion d=1404 x=1241 y=502.9 r=7.521 s=1.248e+04


def Validate(raw, events, gestures):
  fuzzy = FuzzyCheck()
  fuzzy.expected = [
    MotionValidator("> 1000"),
  ]
  fuzzy.unexpected = [
    FlingStopValidator("<=2"),
  ]
  return fuzzy.Check(gestures)
