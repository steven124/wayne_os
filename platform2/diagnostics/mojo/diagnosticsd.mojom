// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// API exposed by the diagnosticsd daemon. Normally the consumer of this API is
// the browser.

// NOTE: This mojom should be kept in sync with the copy in Chromium's repo.

module chromeos.diagnostics.mojom;

// Interface exposed by the diagnosticsd daemon.
interface DiagnosticsdService {
  // Performs initialization, with passing a client handle that allows for
  // bidirectional communication. This should be called before calling any other
  // methods on the interface.
  Init@0(DiagnosticsdServiceClient client_ptr) => ();

  // Sends a message, originating from the diagnostics UI extension (hosted by
  // the browser), to the diagnostics_processor daemon. The message contents are
  // serialized JSON. Delivery of the message is not guaranteed (for example, if
  // the diagnostics_processor daemon isn't running at the moment).
  SendUiMessageToDiagnosticsProcessor@1(handle<shared_buffer> json_message)
      => ();
};

// Interface exposed by the consumer of DiagnosticsdService.
interface DiagnosticsdServiceClient {
  // Sends a message, originating from the diagnostics_processor daemon, to the
  // diagnostics UI extension (hosted by the browser). The message contents are
  // serialized JSON. Delivery of the message is not guaranteed (for example, if
  // the diagnostics UI extension isn't running at the moment).
  SendDiagnosticsProcessorMessageToUi@0(handle<shared_buffer> json_message)
      => ();
};
