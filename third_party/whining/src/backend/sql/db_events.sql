# Copyright 2016 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# MySQL event scheduler is like an internal cron daemon. By default it is off.
# In order to turn it on, do either of:
# - run: SET GLOBAL event_scheduler = ON;
# - launch mysqld with --event-scheduler=ON option
# - set event-scheduler=ON in my.cnf
#
# http://dev.mysql.com/doc/refman/5.5/en/events-configuration.html


delimiter $$
DROP EVENT IF EXISTS find_missing$$
CREATE EVENT find_missing
    ON SCHEDULE
      EVERY 10 MINUTE
    COMMENT 'Run all the find_missing_* SPs'
    DO
      BEGIN
        CALL find_missing_suites(utc_timestamp());
        CALL find_missing_tests(utc_timestamp());
      END
$$
delimiter ;
