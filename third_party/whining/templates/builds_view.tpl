%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%def body_block():
  %# --------------------------------------------------------------------------
  %# Switcher toolbar allows query parameter removal.
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='builds')

  %# --------------------------------------------------------------------------
  %# Builds
  %if not tpl_vars.get('builds'):
    <h3><u>No test builds found.</u></h3>
  %else:
    <div id="divBuilds" class="lefted">
      <table class="alternate_background">
        <tbody>
          <tr>
            <th class="headeritem lefted">Release</th>
            <th class="headeritem lefted">Build</th>
          </tr>
          %for release, build in tpl_vars['builds']:
            <tr>
              <td class="lefted">{{! release }}</td>
              <td class="lefted">{{! build }}</td>
            </tr>
          %end
        </tbody>
      </table>
    </div>
  %end
%end

%rebase('master.tpl', title='builds', query_string=query_string, body_block=body_block)
