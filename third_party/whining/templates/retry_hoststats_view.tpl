%# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%def body_block():
  %# --------------------------------------------------------------------------
  %# Switcher bar for releases (milestones).
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base=view)

  %#-------------------------------------------------------------------------
  %# The table.
  %#-------------------------------------------------------------------------
  %_tbl = tpl_vars['tbl']
  {{!_tbl.html()}}
  <div>
    <h5> Notes: </h5>
    <p>What does <strong>retry pass rate for a host</strong> mean?</p>
      <ul>
        <li>A 10% retry pass rate indicates 1 out of 10 failing test runs
            </br> on this host that were later retried on other hosts
            was finally able to pass.
        </li>
      </ul>
    <p> Only hosts that meet the following criteria are shown: </p>
    <ul>
      <li> retry pass rate higher than 20%. </li>
      <li> at least 5 tests on this host have been retried. </li>
   </ul>
  </div>
%end

%rebase('master.tpl', title=title, query_string=query_string, body_block=body_block)
