%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%def body_block():
  %# --------------------------------------------------------------------------
  %# Switcher toolbar allows query parameter removal.
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='tests')

  %# --------------------------------------------------------------------------
  %# Tests
  %if not tpl_vars.get('tests'):
    <h3><u>No tests found.</u></h3>
  %else:
    <div id="divTests" class="lefted">
      <table class="alternate_background">
        <tbody>
          <tr>
            <th class="headeritem centered">#</th>
            <th class="headeritem lefted">
              Test Name -  {{ len(tpl_vars['tests']) }} total tests
            </th>
          </tr>
          %i = 0
          %for test in tpl_vars['tests']:
            <tr>
              <td class="centered">{{ i }}</td>
              <td class="lefted">{{! test }}</td>
            </tr>
            %i += 1
          %end
        </tbody>
      </table>
    </div>
  %end
%end

%rebase('master.tpl', title='tests', query_string=query_string, body_block=body_block)
