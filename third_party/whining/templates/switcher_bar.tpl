%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.
%
%from src import settings
%_root = settings.settings.relative_root
%
%# --------------------------------------------------------------------------
%# Home - [Releases] - [Query Parameters] - Help
<div>
<table>
  <tbody>
  <tr>
    <th class="headeritem centered">
      <a class="tooltip"
         onmouseover="fixTooltip(event, this)"
         onmouseout="clearTooltip(this)"
         href="{{ _root }}/">
        Home
        <span>
          Return to Home.
        </span>
      </a>
    </th>

    %if tpl_vars.get('filter_tag') and tpl_vars['filter_tag'] not in ['unfiltered', 'lookups']:
      <th class="headeritem centered">
        <a class="tooltip"
           onmouseover="fixTooltip(event, this)"
           onmouseout="clearTooltip(this)"
           href="{{ _root }}/{{ tpl_vars['filter_tag'] }}">
          Up
          <span>
            Return to main view with this filter.
            %if tpl_vars['filter_tag'] == 'unfiltered':
              WARNING: this WILL be lengthy for 'unfiltered'.
            %end
          </span>
        </a>
      </th>
    %end

    %if len(url_base) > 0:
      <th class="headeritem centered">View: {{ url_base }}</th>
      %if url_base[0] != '/':
        %url_base = '/' + url_base
      %end
    %end

    %if tpl_vars.get('releases') and tpl_vars.get('filter_tag') is not None:
      <td>
        %for r in tpl_vars['releases']:
            <a class="tooltip"
               onmouseover="fixTooltip(event, this)"
               onmouseout="clearTooltip(this)"
               href="{{ _root }}{{ url_base }}/{{ tpl_vars['filter_tag'] }}{{! query_string({'releases': r}) }}">
              R{{ r }}
              <span>
                Show {{ url_base }} for release: R{{ r }}.
              </span>
            </a>
            &nbsp;
        %end
      </td>
    %end

    %if tpl_vars.get('filter_tag'):
      %_qps = query_string(remove=['embedded'], list_qps=True)
      %for _qp in _qps:
      <td class="lefted">
        %new_url = query_string(remove=[_qp[0]])
        <a class = "tooltip"
           href="{{ _root }}{{ url_base }}/{{ tpl_vars['filter_tag'] }}{{! new_url }}"
           onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)">
           [x]
           <span style="width:300px;">
             Refresh without {{ _qp[0] }}={{ _qp[1] }}.
           </span>
        </a>
        {{ _qp[0] }}={{ _qp[1].replace(',', ', ') }}
      </td>
      %end
    %end
    <th class="headeritem centered">
      <a class="tooltip" href="{{ _root }}/help"
         onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)">
        Help
        <span>
          Get Help.
        </span>
      </a>
    </th>
  </tr>
  </tbody>
</table>
</div>
