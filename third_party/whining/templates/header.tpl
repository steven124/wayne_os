%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.
%
%import time
%from src import settings
%_root = settings.settings.relative_root
%
<div class="timings">
  Generated: {{ time.strftime('%b %d, %H:%M:%S UTC', time.gmtime()) }}
</div>
<div class="maia-header" id="maia-header" role="banner">
  <div class="maia-aux">
    <h1>
      <a href="{{ _root }}/">
      <img alt="Google"
           src="{{ _root }}/static/logo.png"></a>
      Chromium OS Test Results
    </h1>
  </div>
</div>
