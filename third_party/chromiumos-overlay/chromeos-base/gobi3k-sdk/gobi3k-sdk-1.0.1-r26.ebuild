# Copyright (c) 2011 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

CROS_WORKON_COMMIT="825fc302e0e0ffeb2aa925da70ebc32bc5377c2b"
CROS_WORKON_TREE="dcd66bf29e5e229528c5ee8ec738b89fd382e560"
CROS_WORKON_PROJECT="chromiumos/third_party/gobi3k-sdk"
CROS_WORKON_LOCALNAME=../third_party/gobi3k-sdk
inherit cros-sanitizers cros-workon toolchain-funcs multilib

DESCRIPTION="SDK for Qualcomm Gobi 3000 modems"

LICENSE="BSD"
SLOT="0"
KEYWORDS="*"
IUSE="-asan"

# TODO(jglasgow): remove realpath dependency
RDEPEND="
	|| ( >=sys-apps/coreutils-8.15 app-misc/realpath )
"

src_configure() {
	sanitizers-setup-env
	cros-workon_src_configure
	tc-export LD CXX CC OBJCOPY AR
	export LIBSUBDIR="$(get_libdir)"
}
