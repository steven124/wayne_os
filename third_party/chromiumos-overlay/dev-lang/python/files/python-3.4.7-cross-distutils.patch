Extensions should be installed to the targets libdir. This is important if e.g. host
has a 64bit /usr/lib64, but the target is 32bit and has $ROOT/usr/lib. Make sure we
respect the target's lib structure by getting the libdir name from Makefile.

--- a/Lib/distutils/command/build_ext.py
+++ b/Lib/distutils/command/build_ext.py
@@ -239,7 +239,8 @@ class build_ext(Command):
         if (sysconfig.get_config_var('Py_ENABLE_SHARED')):
             if not sysconfig.python_build:
                 # building third party extensions
-                self.library_dirs.append(sysconfig.get_config_var('LIBDIR'))
+                sysroot = os.getenv('SYSROOT', '')
+                self.library_dirs.append(sysroot + sysconfig.get_config_var('LIBDIR'))
             else:
                 # building python standard extensions
                 self.library_dirs.append('.')
--- a/Lib/distutils/command/install.py
+++ b/Lib/distutils/command/install.py
@@ -29,8 +29,8 @@ WINDOWS_SCHEME = {
 
 INSTALL_SCHEMES = {
     'unix_prefix': {
-        'purelib': '$base/@@GENTOO_LIBDIR@@/python$py_version_short/site-packages',
-        'platlib': '$platbase/@@GENTOO_LIBDIR@@/python$py_version_short/site-packages',
+        'purelib': '$base/$libdirname/python$py_version_short/site-packages',
+        'platlib': '$platbase/$libdirname/python$py_version_short/site-packages',
         'headers': '$base/include/python$py_version_short$abiflags/$dist_name',
         'scripts': '$base/bin',
         'data'   : '$base',
@@ -311,6 +311,7 @@ class install(Command):
         # everything else.
         self.config_vars['base'] = self.install_base
         self.config_vars['platbase'] = self.install_platbase
+        self.config_vars['libdirname'] = self.install_libdirname
 
         if DEBUG:
             from pprint import pprint
@@ -425,6 +426,10 @@ class install(Command):
 
             self.install_base = self.prefix
             self.install_platbase = self.exec_prefix
+            self.install_libdirname = os.path.basename(get_config_vars('LIBDIR')[0])
+            if self.install_libdirname is None:
+                self.install_libdirname = '@@GENTOO_LIBDIR@@'
+
             self.select_scheme("unix_prefix")
 
     def finalize_other(self):
--- a/Lib/distutils/sysconfig.py
+++ b/Lib/distutils/sysconfig.py
@@ -16,11 +16,18 @@ import sys
 from .errors import DistutilsPlatformError
 
 # These are needed in a couple of spots, so just compute them once.
+SYSROOT = os.getenv('SYSROOT', '')
 PREFIX = os.path.normpath(sys.prefix)
 EXEC_PREFIX = os.path.normpath(sys.exec_prefix)
 BASE_PREFIX = os.path.normpath(sys.base_prefix)
 BASE_EXEC_PREFIX = os.path.normpath(sys.base_exec_prefix)
 
+# Make sure we respect the user specified SYSROOT environment variable.
+# This is the first step to get distutils to crosscompile stuff.
+if SYSROOT:
+    PREFIX = os.path.normpath(SYSROOT + os.path.sep + PREFIX)
+    EXEC_PREFIX = os.path.normpath(SYSROOT + os.path.sep + EXEC_PREFIX)
+
 # Path to the base directory of the project. On Windows the binary may
 # live in project/PCBuild9.  If we're dealing with an x64 Windows build,
 # it'll live in project/PCbuild/amd64.
@@ -133,6 +140,12 @@ def get_python_lib(plat_specific=0, standard_lib=0, prefix=None):
 
     If 'prefix' is supplied, use it instead of sys.base_prefix or
     sys.base_exec_prefix -- i.e., ignore 'plat_specific'.
+
+    For the posix system we can not always assume the host's notion of the
+    libdir is the same for the target.  e.g. compiling on an x86_64 system
+    will use 'lib64' but an arm 32bit target will use 'lib'.  So encode all
+    the known lists of dirs and search them all (starting with the host one
+    so that native builds work just fine).
     """
     if prefix is None:
         if standard_lib:
@@ -141,8 +154,19 @@ def get_python_lib(plat_specific=0, standard_lib=0, prefix=None):
             prefix = plat_specific and EXEC_PREFIX or PREFIX
 
     if os.name == "posix":
-        libpython = os.path.join(prefix,
-                                 "@@GENTOO_LIBDIR@@", "python" + get_python_version())
+        # Search known Gentoo vars first.
+        libpython = None
+        if SYSROOT:
+            abi = os.environ.get('ABI')
+            libdir = os.environ.get('LIBDIR_%s' % abi)
+            if libdir:
+                libpython = os.path.join(prefix, libdir, "python" + get_python_version())
+        if not libpython:
+            # Fallback to hardcoded search.
+            for libdir in ['@@GENTOO_LIBDIR@@', 'lib64', 'lib32', 'libx32', 'lib']:
+                libpython = os.path.join(prefix, libdir, "python" + get_python_version())
+                if os.path.exists(libpython):
+                    break
         if standard_lib:
             return libpython
         else:
