/*
 *	Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
 *	Use of this source code is governed by a BSD-style license that can be
 *	found in the LICENSE file.
 */

/*
 * NAME
 *	cros_sample_getcwd02
 *
 * DESCRIPTION
 *	Testcase to check the basic functionality of the getcwd(2) system call.
 *
 *	This was copied from the vanilla LTP getcwd test that resides under
 *	testcases/kernel/syscalls/getcwd. It has been simplified to serve the
 *	purpose of being a sample .c test within the cros tree.
 *
 * ALGORITHM
 *	Get the path name of the current working directory from the current
 *	shell through a pipe, and compare it with what is returned by
 *	getcwd(2) system call.
 *
 *	Block 1: Call getcwd(2) with valid cwd[]:
 *							Should work fine
 *
 *	Block 5: Call getcwd(2) with *buffer = NULL, size = 0:
 *		 Should allocate buffer, and work fine
 *
 *	FULL REFERENCE:
 *		http://ltp.sourceforge.net/documentation/how-to/ltp.php
 *		Contents and organization of LTP.
 *		Instructions for creating tests including:
 *		-.c test template
 *		-Bash test template
 *
 *	QUICK REFERENCE FOR TEST WRITING:
 *	1. TCID, TST_TOTAL and tst_count must be defined for the harness.
 *	2. The test is run in a tmpdir, use setup() and cleanup() for this.
 *	3. tst_resm() returns 0 for success and nonzero for fail.
 *	4. Use these command line api's to indicate test status:
 *
 *		 Informational messages:
 *			 tst_resm(TINFO, "Informational message...")
 *
 *		 PASSED test:
 *			 tst_resm(TPASS, "Test succeeded message...");
 *
 *		 FAILED test:
 *			 tst_resm(TFAIL, "Test failed message...");
 *
 *		 Unexpected infrastructure problem:
 *			 tst_brkm(TBROK, <cleanup_fn or NULL>, "Unexpected ... message");
 *
 *		 Flavors of these commands exist that can also emit a file to include
 *		 detailed status as follows:
 *			 tst_res(<status>, <fname>, "tmessage...")
 *			 tst_brk(TBROK, <fname>, <cleanup_fn>, "tmessage...")
 *
 *			 <tstatus> is one of:
 *				 TINFO - informational message
 *				 TPASS - test passed
 *				 TFAIL - test failed
 *				 TWARN - warning during the test
 *	6. Add your test to $LTPROOT/runtest/cros_kernel_unittests
 */
#include <stdio.h>
#include <string.h>
#include "test.h"
#include "usctest.h"
#define FAILED 1

char *pwd = "/bin/pwd";
int flag;
char *TCID = "cros_sample_getcwd02";
int TST_TOTAL = 2;

void cleanup(void);
void setup(void);
void do_block1(void);
void do_block5(void);

char pwd_buf[BUFSIZ];  //holds results of pwd pipe
char cwd[BUFSIZ];			 //used as our valid buffer
char *buffer = NULL;	//catches the return value from getcwd when passing NULL
char *cwd_ptr = NULL;  //catches the return value from getcwd when passing cwd[]

int main(int ac, char **av)
{
	FILE *fin;
	char *cp;
	int lc;			/* loop counter */
	const char *msg;	/* parse_opts() return message */

	if ((msg = parse_opts(ac, av, NULL, NULL)) != NULL) {
		tst_brkm(TBROK, NULL, "OPTION PARSING ERROR - %s", msg);
	}
	setup();

	/*
	 * The following loop checks looping state if -i option given
	 */
	for (lc = 0; TEST_LOOPING(lc); lc++) {
		tst_count = 0;

		if ((fin = popen(pwd, "r")) == NULL) {
			tst_resm(TINFO, "%s: can't run %s", TCID, pwd);
			tst_brkm(TBROK, cleanup, "%s FAILED", TCID);
		}
		while (fgets(pwd_buf, sizeof(pwd_buf), fin) != NULL) {
			if ((cp = strchr(pwd_buf, '\n')) == NULL) {
				tst_brkm(TBROK, cleanup, "pwd output too long");
			}
			*cp = 0;
		}
		pclose(fin);

		do_block1();
		do_block5();
	}
	cleanup();
	tst_exit();
}

void do_block1(void)		//valid cwd[]: -> Should work fine
{
	int flag = 0;
	tst_resm(TINFO, "Enter Block 1");

	if ((cwd_ptr = getcwd(cwd, sizeof(cwd))) == NULL) {
		tst_resm(TFAIL|TERRNO, "getcwd() failed unexpectedly.\n");
		flag = FAILED;
	}
	if ((flag != FAILED) && (strcmp(pwd_buf, cwd) != 0)) {
		tst_resm(TFAIL, "getcwd() returned unexpected working "
			 "directory: expected: %s, got: %s\n", pwd_buf, cwd);
		flag = FAILED;
	}
	tst_resm(TINFO, "Exit Block 1");
	if (flag == FAILED) {
		tst_resm(TFAIL, "Block 1 FAILED");
	} else {
		tst_resm(TPASS, "Block 1 PASSED");
	}
}

void do_block5(void)		//buffer = NULL, and size = 0, should succeed
{
	int flag = 0;
	tst_resm(TINFO, "Enter Block 5");

	if ((buffer = getcwd(NULL, 0)) == NULL) {
		tst_resm(TFAIL|TERRNO, "getcwd() failed unexpectedly.\n");
		flag = FAILED;
	}
	if ((flag != FAILED) && (strcmp(pwd_buf, buffer) != 0)) {
		tst_resm(TFAIL, "getcwd() returned unexpected working "
			 "directory: expected: %s, got: %s\n", pwd_buf, buffer);
		flag = FAILED;
	}
	tst_resm(TINFO, "Exit Block 5");
	if (flag == FAILED) {
		tst_resm(TFAIL, "Block 5 FAILED");
	} else {
		tst_resm(TPASS, "Block 5 PASSED");
	}
	free(buffer);
	buffer = NULL;
}

void setup(void)
{
	/* FORK is set here because of the popen() call above */
	tst_sig(FORK, DEF_HANDLER, cleanup);

	TEST_PAUSE;

	/* create a test directory and cd into it */
	tst_tmpdir();
}

void cleanup(void)
{
	/* remove the test directory */
	tst_rmdir();
}
